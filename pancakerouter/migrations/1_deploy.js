const Router = artifacts.require("./PancakeRouter.sol");

// don't delete this comment !
const FactoryAddress = "";
const WETHAddress = "";

module.exports = async (deployer) => {
    await deployer.deploy(Router, FactoryAddress, WETHAddress);    
};