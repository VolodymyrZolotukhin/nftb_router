const Router = artifacts.require("./PancakeRouter.sol");
const Factory = artifacts.require("./PancakeRouter.sol/IPancakeFactory");
const Pair = artifacts.require("./PancakeRouter.sol/IPancakePair");

const TestCoin = artifacts.require("./mocks/ERC20Mock.sol");

console.log("\nganache command : ganache-cli -f https://data-seed-prebsc-1-s1.binance.org:8545 --unlock 0x76e98f7d84603AEb97cd1c89A80A9e914f181679\n");


contract("PancakeRouter", async(accounts) => {
    const OWNER = accounts[0];
    
    const ONE_ETH = web3.utils.toWei("1", "ether");
    const WETH = "0xA009003BA2c6Af4C77cD1E6c3512723EC2Bdb5eE";
    const MASTERCHEF = "0x0B9C525a6051fD2E9B8F93d47e23aa2023574eA4";
    const ROUTER = "0xc20d50CA8e0FA3B189c2DdCA3e422E65a0A88397";

    const timestamp = () =>{
        return new Date().getTime();
    }

    let router;
    let testCoint;
    let factory;

    before("getting from testnet", async() => {
        router = await Router.at(ROUTER);
        factory = await Factory.at(await router.factory());
        testCoint = await TestCoin.new();
        await testCoint.mint(ONE_ETH);
    });

    describe("itegration", async() => {
        it("should add liqETH and approve masterChef address to pair", async() => {
            await testCoint.approve(router.address, ONE_ETH);
            await router.addLiquidityETH(testCoint.address, ONE_ETH, 0, 0, OWNER, timestamp(), {value:ONE_ETH});
            let pair = await Pair.at(await factory.getPair(testCoint.address, WETH));
            await pair.approve(MASTERCHEF, (await pair.balanceOf(OWNER)));
            console.log("Pair address : "+pair.address);
        });
    });
});