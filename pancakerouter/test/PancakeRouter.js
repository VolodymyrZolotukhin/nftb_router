const Router = artifacts.require("./PancakeRouter.sol");
const Factory = artifacts.require("./PancakeRouter.sol/IPancakeFactory");
const Pair = artifacts.require("./PancakeRouter.sol/IPancakePair");
const ERC20Mock = artifacts.require("./ERC20Mock.sol");
const WETHMock = artifacts.require("./WETHMock.sol/WBNB");

const TestCoin = artifacts.require("./mocks/ERC20Mock.sol");
const { advanceBlock } = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");

contract("PancakeRouter", async(accounts) => {
    const OWNER = accounts[0];
    const FACTORY = "0xe6E374418F36F1BBc0861c391929F4346200594B";
    const BaseCoin = "0x003CC78F9A54d6ce82408FE0776F08DE6523CdfD";
    const ONE_ETH = web3.utils.toWei("1", "ether");
    const DECIMALS = 18;

    function toBN(str){
        return web3.utils.toBN(str);
    }

    const timestamp = () =>{
        return new Date().getTime();
    }

    const reverter = new Reverter(web3);

    let router;
    let testCoin;
    let factory;
    let weth;
    let busd;
    let baseCoin;

    before("setup", async() => {
        testCoin = await TestCoin.new("testCoin","TS",DECIMALS);
        await testCoin.mint(OWNER,ONE_ETH);
                
        weth = await WETHMock.new();
        busd = await ERC20Mock.new("BUSD", "BUSD",DECIMALS);

        factory = await Factory.at(FACTORY);
        router = await Router.new(factory.address, weth.address);

        baseCoin = await ERC20Mock.at(BaseCoin);
        await baseCoin.mint(OWNER, ONE_ETH);

        await weth.deposit({value: ONE_ETH});
        await busd.mint(OWNER, ONE_ETH);

        await baseCoin.approve(router.address, ONE_ETH);
        await testCoin.approve(router.address, ONE_ETH);
        await weth.approve(router.address, ONE_ETH);
        await busd.approve(router.address, ONE_ETH);

        await reverter.snapshot();
    });

    afterEach("revert", reverter.revert);

    describe("addLiquidity", async() => {
        it("should add 1001 Liquidity and create new Pair", async() => {
            const amount = 1001;
            
            let testCoinBefore = await testCoin.balanceOf(OWNER);
            let baseCoinBefore = await baseCoin.balanceOf(OWNER);

            await router.addLiquidity(testCoin.address, baseCoin.address, amount, amount, 1, 1, OWNER, timestamp()+1000);

            let pair = await Pair.at(await factory.getPair(testCoin.address, baseCoin.address));

            let pairTokens = [await pair.token0(), await pair.token1()];

            assert.include(pairTokens, testCoin.address);
            assert.include(pairTokens, baseCoin.address);
           
            assert.equal(testCoinBefore.sub(toBN(amount)).toString(), (await testCoin.balanceOf(OWNER)).toString());
            assert.equal(baseCoinBefore.sub(toBN(amount)).toString(), (await baseCoin.balanceOf(OWNER)).toString());

            assert.equal(amount, (await testCoin.balanceOf(pair.address)));
            assert.equal(amount, (await baseCoin.balanceOf(pair.address)));

        });
    });

    describe("removeLiquidity", async() => {
        it("should remove 100 liquditity", async() => {
            const amount = 10010;
            const removeAmount = 10;

            await router.addLiquidity(testCoin.address, baseCoin.address, amount, amount, 1, 1, OWNER, timestamp()+1000);
            
            let testCoinBefore = await testCoin.balanceOf(OWNER);
            let baseCoinBefore = await baseCoin.balanceOf(OWNER);
            
            let pair = await Pair.at(await factory.getPair(testCoin.address, baseCoin.address));
            await pair.approve(router.address, removeAmount);

            let pairTestCoin = await testCoin.balanceOf(pair.address);
            let pairBaseCoin = await baseCoin.balanceOf(pair.address);

            await router.removeLiquidity(testCoin.address, baseCoin.address, removeAmount, removeAmount, removeAmount, OWNER, timestamp()+1000);
             
            assert.equal(testCoinBefore.add(toBN(removeAmount)).toString(), (await testCoin.balanceOf(OWNER)).toString());
            assert.equal(baseCoinBefore.add(toBN(removeAmount)).toString(), (await baseCoin.balanceOf(OWNER)).toString());

            assert.equal(removeAmount, pairTestCoin.sub((await testCoin.balanceOf(pair.address))));
            assert.equal(removeAmount, pairBaseCoin.sub((await baseCoin.balanceOf(pair.address))));

        });
    });

    describe("addLiquidityETH", async() => {
        it("should add Liquidity with WETH", async() => {
            const amount = 1001;
            
            let baseCoinBefore = await baseCoin.balanceOf(OWNER);

            await router.addLiquidityETH(baseCoin.address, amount, 1, 1, OWNER, timestamp()+1000,{value: amount});

            let pair = await Pair.at(await factory.getPair(weth.address, baseCoin.address));

            assert.equal(baseCoinBefore.sub(toBN(amount)).toString(), (await baseCoin.balanceOf(OWNER)).toString());

            assert.equal(amount, (await weth.balanceOf(pair.address)));
            assert.equal(amount, (await baseCoin.balanceOf(pair.address)));

        });
    });

    describe("removeLiquidityETH", async() => {
        it("should remove 10 ETH Liquidity", async() => {
            const amount = 10010;
            const removeAmount = 10;

            await router.addLiquidityETH(baseCoin.address, amount, 1, 1, OWNER, timestamp()+1000,{value: amount});

            let pair = await Pair.at(await factory.getPair(weth.address, baseCoin.address));
            await pair.approve(router.address, removeAmount);

            let pairWeth = await weth.balanceOf(pair.address);
            let pairBaseCoin = await baseCoin.balanceOf(pair.address);
            
            let ethBefore = await web3.eth.getBalance(OWNER);
            let baseCoinBefore = await baseCoin.balanceOf(OWNER);

            let tx = (await router.removeLiquidityETH(baseCoin.address, removeAmount, removeAmount, removeAmount, OWNER, timestamp()+1000));
            
            assert.equal(toBN(ethBefore).add(toBN(removeAmount)).sub(toBN(tx.receipt.gasUsed)).toString(), (await web3.eth.getBalance(OWNER)).toString());
            assert.equal(baseCoinBefore.add(toBN(removeAmount)).toString(), (await baseCoin.balanceOf(OWNER)).toString());

            assert.equal(removeAmount, pairWeth.sub((await weth.balanceOf(pair.address))));
            assert.equal(removeAmount, pairBaseCoin.sub((await baseCoin.balanceOf(pair.address))));
        });
    });

    describe("swap", async() => {
        it("should swap tokens for exact tokens correctly", async() => {
            const amount = 10010;
            await router.addLiquidity(testCoin.address, baseCoin.address, amount, amount, 1, 1, OWNER, timestamp()+1000);
            
            let baseCoinBefore = await baseCoin.balanceOf(OWNER);
            let testCoinBefore = await testCoin.balanceOf(OWNER);

            let amounts = await router.getAmountsOut(100, [testCoin.address, baseCoin.address]);
            
            await router.swapTokensForExactTokens(amounts[1], amounts[0], [testCoin.address, baseCoin.address], OWNER, timestamp()+1000);
            
            assert.equal(testCoinBefore.sub(await testCoin.balanceOf(OWNER)).toString(), amounts[0].toString());
            assert.equal((await baseCoin.balanceOf(OWNER)).sub(baseCoinBefore).toString(), amounts[1]);
            
        });
    });
});