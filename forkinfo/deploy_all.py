import os

FILE_NAME = "addresses.txt" # filename to save addresses
OUTPUT_FILE = "output.txt" # file to save temporary data

weth = "" #weth address
baseCoin = "" #baseCoin for factory
feeToSetter = "" #feeToSetter for factory
devaddr = "" # devaddr for masterchef
network = "" # network to deploy
witoutFactory = False # if True, factory address gets from FILE_NAME. This option for continue deploing after init hash error

def find_and_replace(path, pattern_value_pairs):
	buffer = None

	with open(path, "r") as f:
		buffer = f.readlines()

	index = 0

	for line in buffer:
		index+=1
		if line.strip() == "// don't delete this comment !":
			break
	
	for pair in pattern_value_pairs:
		buffer[index] = pair[0]+'"'+pair[1]+'";\n'
		index+=1 

	with open(path, "w") as f:
		f.writelines(buffer)


def save_to_deployed(name):
	with open(OUTPUT_FILE, "r") as output_file:
		file = output_file.readlines()
		with open(FILE_NAME, "a+") as deployed:
			for i in range(0,len(name)):
				deployed.write(name[i]+" : " + file[i].split(' ')[-1])


def clear_file(name):
	with open(name, "w") as f:
		pass

if feeToSetter == "" or devaddr == "" or baseCoin == "":
	print("Empty addresses. Check README.md")
	exit()

lines = None

if not witoutFactory:
	print("Deploing Factory")

	clear_file(FILE_NAME)
	clear_file(OUTPUT_FILE)

	find_and_replace("../pancakefactory/migrations/1_deploy.js", [('const feeToSetter = ', feeToSetter), ("const baseCoin = ", baseCoin)])
	os.system('./deploy_from_folder.sh pancakefactory {0}| grep -e "init hash :" -e "contract address:" > {1}'.format(network, OUTPUT_FILE))

	save_to_deployed(["factory"])

	print("Deployed")

	with open(OUTPUT_FILE,"r") as f:
		lines = f.readlines()
		init_hash = "                hex'{}' // init code hash\n".format(lines[-1].split(' ')[-1][2:-1])

		with open("../pancakerouter/contracts/PancakeRouter.sol") as contract_code:
			line = contract_code.readlines()[297];
			
			if init_hash != line:
				print("Init hash error. Actual hash is {}".format(lines[-1].split(' ')[-1][2:-1]))
				if input("Continue? [y/n] : ").lower() == 'n':
					exit()
else:
	with open(FILE_NAME,"r") as f:
		lines = f.readlines()

print("Deploing Router")
clear_file(OUTPUT_FILE)
find_and_replace("../pancakerouter/migrations/1_deploy.js", [('const FactoryAddress = ', lines[0].split(' ')[-1][:-1]), ('const WETHAddress = ', weth)])
os.system('./deploy_from_folder.sh pancakerouter {0} | grep "contract address:" > {1}'.format(network, OUTPUT_FILE))
save_to_deployed(["router"])
clear_file(OUTPUT_FILE)
print("Deployed")


print("Deploing Cake, Syrup, MasterChef")
find_and_replace("../masterchef/migrations/1_deploy.js", [("const devaddr = ", devaddr)])
os.system('./deploy_from_folder.sh masterchef {0} | grep "contract address:" > {1}'.format(network, OUTPUT_FILE))
save_to_deployed(["CakeToken","SyrupBar","MasterChef"])
clear_file(OUTPUT_FILE)
print("Deployed")


print("Deploing SmartChefFactory")
os.system('./deploy_from_folder.sh smartchefinitializable {0} | grep "contract address:" > {1}'.format(network, OUTPUT_FILE))
save_to_deployed(["SmartChefFactory"])
clear_file(OUTPUT_FILE)
print("Deployed")

print("Finished!")

print("\n Addresses saved to {}".format(FILE_NAME))