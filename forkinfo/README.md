Image "ContractDependencies.png" shows relationship of contracts.
The image is divided into tree parts: Router ecosystem (Right), SmartChef ecosystem (Left) and MasterChef ecosystem (Bottom).

All links to contracts:
 - MasterChef: https://bscscan.com/address/0x73feaa1ee314f8c655e354234017be2193c9e24e#code
 - CakeToken: https://bscscan.com/address/0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82#code
 - SyrupBar: https://bscscan.com/address/0x009cf7bc57584b7998236eff51b98a168dcea9b0#code

 - Factory: https://bscscan.com/address/0xca143ce32fe78f1f7019d7d551a6402fc5350c73#code
 - Router: https://bscscan.com/address/0x10ed43c718714eb63d5aa57b78b54704e256024e#code
    
 - SmartChefFactory: https://bscscan.com/address/0x927158be21fe3d4da7e96931bb27fd5059a8cbc2#code

Actual links placed here: https://docs.pancakeswap.finance/code/smart-contracts

Deploy order:

  For router ecosystem:
   1. Deploy factory.
   2. Check match of INIT_CODE_PAIR_HASH of factory with code, pasted in router contract. Replace if not. CTRL+F "init code hash" to find place to paste.
   3. Deploy router with factory's address.

  For masterChef ecosystem:
   1. Deploy CakeToken.
   2. Deploy SurypBar with cakeToken's address.
   3. Deploy MasterChef with cakeToken's and syrup's addresses.
   4. CakeToken: transferOwnership to MasterChef.
   5. SyrupBar: transferOwnership to MasterChef.

  For smartChef ecosystem:
   1. Deploy SmartChefFactory.


Deploy script guide:
  1. Run npm install in all folders
  2. Check deploy_all.py and enter addresses for devaddr, feeToSetter and other fields
  3. In deploy_all.py enter name of network
  4. Check PancakeRouter/migrations/1_deploy.js and enter our names of networks if needed
  5. run script from this folder with command : python3 deploy_all.py