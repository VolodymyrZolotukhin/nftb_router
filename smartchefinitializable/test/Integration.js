const { advanceBlock } = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");

const Factory = artifacts.require("./SmartChefFactory.sol");
const Initialize = artifacts.require("./SmartChefFactory.sol/SmartChefInitializable");
const IBEP20 = artifacts.require("./SmartChefFactory.sol/IBEP20");
const TestCoin = artifacts.require("./mocks/ERC20Mock.sol");


const FACTORY = "0xCF5b96B51F3AFC3169a77Db87A697B51a080a4f3";
const CAKE = "0xc9192a559B8c489c371AADF26e1911287A76e58E";

const USER = "0x76e98f7d84603AEb97cd1c89A80A9e914f181679"; //owner's address

const reverter = new Reverter(web3);

contract("SmartChefFactory", async(accounts) => {
    const OWNER = accounts[0];
    const ALICE = accounts[1];

    let factory;
    let cake;
    let testCoin;

    before("getting from testnet", async() => {
        factory = await Factory.at(FACTORY);
        cake = await IBEP20.at(CAKE);
        testCoin = await TestCoin.new()
        await testCoin.mint(1000);
    });

    let smartChef

    describe("Integration", async() => {
        it("Create pool", async() => {
            let tx = await factory.deployPool(cake.address, testCoin.address, 1, 1, (await web3.eth.getBlock("latest")).number+50, 1000, OWNER,{from:USER});
            let addr = tx.logs[2].args.smartChef;
            smartChef = await Initialize.at(addr);
            assert.equal((await smartChef.poolLimitPerUser()).toString(), "1000");
            await testCoin.transfer(smartChef.address, 1000);
          /*  console.log("smartChef : " + smartChef.address);
            console.log("testCoin : " + testCoin.address);*/
        });

        it("Stake testCoin for cake", async() => {
            await cake.approve(smartChef.address, 1000);
            await smartChef.deposit(1000);
            assert.equal((await testCoin.balanceOf(OWNER)).toString(), 0);
            await advanceBlock();
            await smartChef.withdraw(500);
            assert.equal("2", (await testCoin.balanceOf(OWNER)).toString());

        });
    });
});