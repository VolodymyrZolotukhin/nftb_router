const Factory = artifacts.require("./SmartChefFactory.sol");
const Initialize = artifacts.require("./SmartChefFactory.sol/SmartChefInitializable");

contract("SmartChefFactory", async(accounts) => {
    const OWNER = accounts[0];

    const WETH = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";
    const BUSD = "0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56";

    let factory;

    before("setup", async() => {
        factory = await Factory.new()
    });

    describe("deployPool", async() => {
        it("should deploy smartChef", async() => {
            let tx = await factory.deployPool(WETH, BUSD, 1, 1, 5, 2, OWNER);
            let addr = tx.logs[2].args.smartChef;
            let smartChef = await Initialize.at(addr);
            assert.equal((await smartChef.poolLimitPerUser()).toString(), "2");
        });
    });
});