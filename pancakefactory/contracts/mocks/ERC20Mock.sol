// SPDX-License-Identifier: MIT
pragma solidity =0.5.16;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ERC20Mock is ERC20 {
    uint8 internal _decimals;

    constructor(
        uint8 decimalPlaces
     ) public ERC20() {
        _decimals = decimalPlaces;
    }

    function decimals() public view returns (uint8) {
        return _decimals;
    }

    function mint(address to, uint256 _amount) public {
        _mint(to, _amount);
    }

    function burn(address to, uint256 _amount) public {
        _burn(to, _amount);
    }
}
