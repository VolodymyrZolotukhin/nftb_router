const Factory = artifacts.require("./PancakeFactory.sol");
const Pair = artifacts.require("./PancakeFactory.sol/IPancakePair");
const ERC20Mock = artifacts.require("ERC20Mock.sol");
const truffleAssert = require("truffle-assertions");

let baseCoin;
let factory;

contract("PancakeRouter", async(accounts) => {

    before("setup", async() => {
        baseCoin = await ERC20Mock.new(18);
        factory = await Factory.new(accounts[0], baseCoin.address);
        await factory.setFeeTo(accounts[0]);
    });

    describe.skip("deploy", async() => {
        it("deploy", async() => {
            console.log("addr : " + factory.address);
            console.log("init hash : " + (await factory.INIT_CODE_PAIR_HASH()).toString());
            console.log("token addr : " + baseCoin.address);
        });
    });

    describe("createPair", async() => {
        it("should create pair", async() => {
            let tokenA  = await ERC20Mock.new(18);
            let addr = (await factory.createPair(tokenA.address, baseCoin.address)).receipt.logs[0]["args"]["pair"];
            
            let pair = await Pair.at(addr);
            
            assert.equal((await factory.getPair(tokenA.address, baseCoin.address)).toString(), addr);
            
            let pairTokens = [await pair.token0(), await pair.token1()];

            assert.include(pairTokens, tokenA.address);
            assert.include(pairTokens, baseCoin.address);
            assert.equal((await pair.factory()).toString(), factory.address);

        });

        it("should revert when try to create pair without base token", async() => {
            let tokenA = await ERC20Mock.new(18);
            let tokenB = await ERC20Mock.new(18);

            await truffleAssert.reverts( factory.createPair(tokenA.address, tokenB.address), "Pancake: can't create pair without base token");
        });
    });
});