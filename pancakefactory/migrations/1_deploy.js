const Factory = artifacts.require('PancakeFactory.sol');

// don't delete this comment !
const feeToSetter = "";
const baseCoin = "";

module.exports = async (deployer) => {
    await deployer.deploy(Factory, feeToSetter, baseCoin);
    let factory = await Factory.deployed();

    console.log("\n ---- init hash : "+ await factory.INIT_CODE_PAIR_HASH());
    console.log(" ---- Note: init hash must be inserted witout 0x \n");
};