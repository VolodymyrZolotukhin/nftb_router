const Syrup = artifacts.require("./MasterChef.sol/SyrupBar");
const Cake = artifacts.require("./MasterChef.sol/CakeToken");
const MasterChef = artifacts.require("./MasterChef.sol/MasterChef");

const rewardPerBlock = "10";
let startBlock = ""; // leave empty for default value = web3.eth.getBlock("latest")

// don't delete this comment !
const devaddr = "";

const onlyMasterChef = false; // set true for deploy only MasterChef
const cakeAddress = ""; // if onlyMasterChef is true, cake address should be entered here
const syrupAddress = ""; // if onlyMasterChef is true, syrup address should be entered here

module.exports = async (deployer) => {
      let cake;
      let syrup;

      if(onlyMasterChef){
            cake = await Cake.at(cakeAddress);
            syrup = await Syrup.at(syrupAddress);
      }else{
            await deployer.deploy(Cake);
            cake = await Cake.deployed();
            
            await deployer.deploy(Syrup, cake.address);
            syrup = await Syrup.deployed();
      }

      if(startBlock == ""){
            startBlock = (await web3.eth.getBlock("latest")).number;
      }

      await deployer.deploy(MasterChef, cake.address, syrup.address, devaddr, rewardPerBlock, startBlock);
      
      if(!onlyMasterChef){
            let masterChef = await MasterChef.deployed();
            await cake.transferOwnership(masterChef.address);
            await syrup.transferOwnership(masterChef.address);
      }
};
