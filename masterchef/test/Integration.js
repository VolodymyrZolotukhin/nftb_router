const Syrup = artifacts.require("./MasterChef.sol/SyrupBar");
const Cake = artifacts.require("./MasterChef.sol/CakeToken");
const MasterChef = artifacts.require("./MasterChef.sol/MasterChef");
const { advanceBlock } = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");

const CAKE = "0xc9192a559B8c489c371AADF26e1911287A76e58E";
const MASTERCHEF = "0x0B9C525a6051fD2E9B8F93d47e23aa2023574eA4";
const PAIR = "0xc4F98330f982973985ac58c66552AAfCfd03Dd04"; // pair's address, changes every test run, get from Integration test of router
const USER = "0x76e98f7d84603AEb97cd1c89A80A9e914f181679"; // owner's address from testnet

console.log("\nganache command : ganache-cli -f https://data-seed-prebsc-1-s1.binance.org:8545 --unlock 0x76e98f7d84603AEb97cd1c89A80A9e914f181679\n");

contract("MasterChef", async(accounts) => {
    const OWNER = accounts[0];
    const reverter = new Reverter(web3);

    let cake;
    let masterChef;

    async function advanceBlocks(number){
        for(let i=0;i<number; i++){
            await advanceBlock();
        }
    }

    before("getting from testnet", async() => {
        cake = await Cake.at(CAKE);
        masterChef = await MasterChef.at(MASTERCHEF);
    });

    describe("integration", async() => {

        it("should stake LP for cakes", async() => {
            await reverter.snapshot();
            await masterChef.add(1000, PAIR, true, {from:USER});
            assert.equal((await masterChef.poolLength()).toString(), "2");

            await masterChef.deposit(1, 1);
            advanceBlocks(10);
            await masterChef.withdraw(1, 1);
            assert.equal("22", (await cake.balanceOf(OWNER)).toString());
            await reverter.revert();
        });
    });
});