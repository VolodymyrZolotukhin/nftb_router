const Syrup = artifacts.require("./MasterChef.sol/SyrupBar");
const Cake = artifacts.require("./MasterChef.sol/CakeToken");
const MasterChef = artifacts.require("./MasterChef.sol/MasterChef");
const Mock = artifacts.require("./mocks/ERC20Mock.sol");
const { advanceBlock } = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");

contract("PancakeRouter", async(accounts) => {
    const OWNER = accounts[0];
    const ALICE = accounts[1];

    const reverter = new Reverter(web3); 

    const AMOUNT = 100;
    let syrup;
    let cake;
    let masterChef;
    let testCoin;
    
    before("setup", async() => {
        cake = await Cake.new();
        syrup = await Syrup.new(cake.address);
        masterChef = await MasterChef.new(cake.address,  syrup.address, OWNER, 10, (await web3.eth.getBlock("latest")).number-1);
        await cake.transferOwnership(masterChef.address);
        await syrup.transferOwnership(masterChef.address);
        testCoin = await Mock.new();
        await testCoin.mint(AMOUNT*10);
        reverter.snapshot();
    });

    afterEach("revert", reverter.revert);
    
    describe("poolLength", async() => {
        it("should get pollLength", async() => {
            assert.equal((await masterChef.poolLength()).toString(), "1");
        });
    });

    describe("simulatin real case", async() => {
        it("real case", async() => { 
            await masterChef.add("2000", testCoin.address, true);
            assert.equal((await masterChef.poolLength()).toString(), "2");

            await testCoin.approve(masterChef.address, AMOUNT);
            assert.equal((await cake.balanceOf(OWNER)).toString(), "0");

            await masterChef.deposit(1, AMOUNT);
            await masterChef.withdraw(1, AMOUNT);
            assert.equal((await cake.balanceOf(OWNER)).toString(), 7);
        });
    });
});