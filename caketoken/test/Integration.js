const Cake = artifacts.require("./CakeToken.sol/CakeToken");
const Force = artifacts.require("./ForceETH.sol/ForceSend");

const CAKE = "0xc9192a559B8c489c371AADF26e1911287A76e58E";
const MASTERCHEF = "0x0B9C525a6051fD2E9B8F93d47e23aa2023574eA4";

// ganache-cli -f https://data-seed-prebsc-1-s1.binance.org:8545 --unlock 0x76e98f7d84603AEb97cd1c89A80A9e914f181679 0x0B9C525a6051fD2E9B8F93d47e23aa2023574eA4

// run before SmartChefFactory test

contract("CakeToken", async(accounts) => {

    let cake;

    it("getting from testnet, mint", async() => {
        let force = await Force.new();
        await force.go(MASTERCHEF,{value:10**10});

        cake = await Cake.at(CAKE);
        await cake.mint(accounts[0], 1000, {from:MASTERCHEF});
    });

});