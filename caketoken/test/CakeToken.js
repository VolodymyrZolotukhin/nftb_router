const Cake = artifacts.require("./CakeToken.sol/CakeToken");
const Reverter = require("./helpers/reverter");

contract("CakeToken", async(accounts) => {
    const OWNER = accounts[0];
    const ALICE = accounts[1];
    const reverter = new Reverter(web3);
    const BALANCE = 10**10;

    let cake;

    before("setup", async() => {
        cake = await Cake.new();
        reverter.snapshot();
    });

    afterEach("revert", reverter.revert);

    describe("mint", async() => {
        it("should mint 10**10", async() => {
            await cake.mint(OWNER, BALANCE);
            assert.equal(BALANCE, (await cake.balanceOf(OWNER)).toString());
        });
    });

    describe("delegate", async() => {
        it("should delegates to ALICE", async() => {
            await cake.delegate(ALICE);
           assert.equal(await cake.delegates(OWNER), ALICE);
        });
    });
});